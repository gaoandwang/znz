var tcity=require("./citys.js");                
Page({
    data: {
        tel:'',                                     //手机号码
        code:'',                                    //验证码
        flag:"获取验证码",
        seconds:60,                                 //倒计时60s
        modalOpened: false,                         //地区选择窗口的开关
        zoomValue:'',                               //input中的地区值
        provinces: [],
        province: "",
        citys: [],
        city: "",
        countys: [],
        county: '',
        value: [0, 0, 0],
        values: [0, 0, 0],
    }, 
    handleTelBlur(e){       //失焦后手机号码的值
        this.setData({
            tel:e.detail.value,
        })
    },
    handleYZMBlur(e){   //失焦后验证码的值
        this.setData({
            code:e.detail.value,
        });
        dd.setStorage({
            key: 'userInfo',
            data: {
              tel: this.data.tel,
              code: this.data.code,
            },
            success: function() {
             // dd.alert({content: '写入成功'});
            }
          });
    },
    handlegetYZM(){  //获取验证码的请求
        var RegExp =/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/;
        if(RegExp.test(this.data.tel)){
            this.setData({
                flag:"60秒后可以再次发送",
            });
            var that=this;
            var timer=setInterval(function(){
                that.setData({
                    seconds:--that.data.seconds,
                });
                if(that.data.seconds<=0){
                    console.log(that.data.seconds)
                    clearInterval(timer);
                    that.setData({
                        flag:"获取验证码",
                        seconds:60,
                    });
                }
            },1000);
            dd.httpRequest({
                url: 'http://app.zhuangneizhu.com/user/gainCode.do',
                method: 'POST',
                data: {
                    mobile:this.data.tel,
                    version:"2.8",
                    appType:"znz"
                },
                dataType: 'json',
                success: function(res) {
                //dd.alert({content: 'success'});
                },
                fail: function(res) {
                //dd.alert({content: 'fail'});
                },
                complete: function(res) {
                dd.hideLoading();
                //dd.alert({content: 'complete'});
                }
            });
        }else{
            dd.alert({content:'请输入正确的号码'})
        } 
    },
    handleLogin(){ //登陆按钮
        dd.httpRequest({
            url: 'http://app.zhuangneizhu.com/user/login.do',
            method: 'POST',
            data: {
                companyName:"",
                mobile:this.data.tel,
                code:this.data.code,
                type:"android",
                version:"2.8",
            },
            dataType: 'json',
            success: function(res) {
              console.log(res)
             if(res.data.code="10020"){
                dd.switchTab({url:'../../pages/index/index'})
             }
            },
            fail: function(res) {
              dd.alert({content: 'fail'});
            },
            complete: function(res) {
              dd.hideLoading();
              dd.alert({content: 'complete'});
            }
        }); 
    },
    openModal() {               //地区选择窗口的打开
        this.setData({
          modalOpened: true,
        });
    },
    handleCancle(){             //地区选择窗口的取消并关闭
        this.setData({
            modalOpened: false,
          });
    },
    handleSure(){               //确定选择区域
        this.setData({
            zoomValue:this.data.provinces[this.data.values[0]]+this.data.citys[this.data.values[1]]+this.data.countys[this.data.values[2]]
        });
    },
    handleChange(e) {
        var val = e.detail.value
        var t = this.data.values;
        var cityData = this.data.cityData;
        if(val[0] != t[0]){
          console.log('province no ');
          const citys = [];
          const countys = [];
          for (let i = 0 ; i < cityData[val[0]].sub.length; i++) {
            citys.push(cityData[val[0]].sub[i].name)
          }
          for (let i = 0 ; i < cityData[val[0]].sub[0].sub.length; i++) {
            countys.push(cityData[val[0]].sub[0].sub[i].name)
          }
          this.setData({
            province: this.data.provinces[val[0]],
            city: cityData[val[0]].sub[0].name,
            citys:citys,
            county: cityData[val[0]].sub[0].sub[0].name,
            countys:countys,
            values: val,
            value:[val[0],0,0]
          })
          return;
        }
        if(val[1] != t[1]){
          console.log('city no');
          const countys = [];
          for (let i = 0 ; i < cityData[val[0]].sub[val[1]].sub.length; i++) {
            countys.push(cityData[val[0]].sub[val[1]].sub[i].name)
          }
          
          this.setData({
            city: this.data.citys[val[1]],
            county: cityData[val[0]].sub[val[1]].sub[0].name,
            countys:countys,
            values: val,
            value:[val[0],val[1],0]
          })
          return;
        }
        if(val[2] != t[2]){
          console.log('county no');
          this.setData({
            county: this.data.countys[val[2]],
            values: val
          })
          return;
        }
      },
      onLoad() {
        var that = this;
        tcity.init(that);
        var cityData = that.data.cityData;
        const provinces = [];
        const citys = [];
        const countys = [];
    
        for(let i=0;i<cityData.length;i++){
          provinces.push(cityData[i].name);
        }
        console.log('省份完成');
        for (let i = 0 ; i < cityData[0].sub.length; i++) {
          citys.push(cityData[0].sub[i].name)
        }
        console.log('city完成');
        for (let i = 0 ; i < cityData[0].sub[0].sub.length; i++) {
          countys.push(cityData[0].sub[0].sub[i].name)
        }
    
        that.setData({
          'provinces': provinces,
          'citys':citys,
          'countys':countys,
          'province':cityData[0].name,
          'city':cityData[0].sub[0].name,
          'county':cityData[0].sub[0].sub[0].name
        })
    }
});
