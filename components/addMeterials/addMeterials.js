const app = getApp();
Page({
    data:{
        organizationId:"",
        userId:"",
    },
    onLoad(){
        this.setData({
            organizationId:app.message.organizations[0].organizationId,
            userId:app.message.userId,
        })
    },
    handleInput(e){
        app.materialsName=e.detail.value
    },
    handleSure(){
        dd.httpRequest({   //进行获取验证码码的接口请求
            url: 'http://app.zhuangneizhu.com/set/addMaterialsType.do',
            method: 'POST',
            data: {
                organizationId:this.data.organizationId,  //暂时这么写
                materialsName:app.materialsName,
                userId:this.data.userId,
                version:app.version,
            },
            dataType: 'json',
            success: function(res) {  //成功回调
                dd.navigateTo({
                    url:'../setMeterials/setMeterials'
                })
            },
            fail: function(res) {     //失败回调
                dd.showToast({
                    content: '添加失败',
                    duration: 1500,
                });
            },
        });
    }
})