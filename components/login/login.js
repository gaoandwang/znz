const app = getApp();
Page({
    data: {
        tel:'',  //手机号码
        code:'',  //验证码
        flag:"获取验证码",
        seconds:60,
        timer:null,
    },
    handleTelInput(e){   //Inpu后手机号码的值
        this.setData({
            tel:e.detail.value,
        })
    },
    handleYZMInput(e){   //Input后验证码的值
        this.setData({
            code:e.detail.value,
        });
    },
    handlegetYZM(){  //获取验证码的请求
        var RegExp =/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/; //手机号码的正则
        if(RegExp.test(this.data.tel)){  //手机号码的判断为true
            this.setData({
                flag:"60秒后可以再次发送",  //将获取验证码转换为60秒后可以再次发送
            });
            var that=this;
            this.data.timer=setInterval(function(){  //开启定时器，进行60s倒计时
                that.setData({
                    seconds:--that.data.seconds,
                });
                if(that.data.seconds<=0){  //如果秒数小等于0，关闭定时器，并且将秒数重置为60s
                    clearInterval(that.data.timer);
                    that.setData({
                        flag:"获取验证码",
                        seconds:60,
                    });
                }
            },1000);
            dd.httpRequest({   //进行获取验证码码的接口请求
                url: 'http://app.zhuangneizhu.com/user/gainCode.do',
                method: 'POST',
                data: {
                    mobile:this.data.tel,
                    version:"3.0",
                    appType:"znz"
                },
                dataType: 'json',
                success: function(res) {  //成功回调

                },
                fail: function(res) {     //失败回调
                    dd.showToast({
                        content: '验证码获取失败',
                        duration: 1500,
                    });
                },
            });
        }else{  //手机号码输入不正确的提示
            dd.showToast({
                content: '请输入正确的号码',
                duration: 1500,
            });
        } 
    },
    handleLogin(){ //登陆按钮
        var that=this;
        dd.httpRequest({
            url: 'http://app.zhuangneizhu.com/user/login.do',
            method: 'POST',
            data: {
                companyName:"",
                mobile:this.data.tel,
                code:this.data.code,
                type:"android",
                version:"3.0",
            },
            dataType: 'json',
            success: function(res) { 
                console.log(res)
                if(res.data.code=="10000"){
                    clearInterval(that.data.timer)
                    dd.switchTab({url:'../../pages/index/index'})  //成功后页面跳转
                    app.message=res.data.data
                }else{
                    dd.showToast({
                        content: '请输入正确的验证码',
                        duration: 1500,
                    });
                }
            },
            fail: function(res) {  //登陆失败回调
                dd.showToast({
                    content: '登陆失败',
                    duration: 1500,
                });
            },
            complete: function(res) {
                // dd.setStorage({
                //     key: 'userInfo',
                //     data: {
                //       tel: that.data.tel,
                //     },
                //     success: function() {
                //      console.log('写入成功')
                //     }
                // });
            }
        }); 
    },
    handleRegister(){  //跳转到注册页面
        dd.navigateTo({url:'../register/register'})
    }
    
});