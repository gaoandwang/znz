const app = getApp();
Page({
    data:{
        organizationId:"",
        userId:"",
        items:[]
    },
    onLoad(){
        this.setData({
            organizationId:app.message.organizations[0].organizationId,
            userId:app.message.userId,
        })
        var that=this;
        dd.httpRequest({   //进行获取验证码码的接口请求
            url: 'http://app.zhuangneizhu.com/set/gainMaterialsType.do',
            method: 'POST',
            data: {
                organizationId:this.data.organizationId,
                materialsName:app.materialsName,
                userId:this.data.userId,
                version:app.version,
                qr:"",
            },
            dataType: 'json',
            success: function(res) {  //成功回调
                console.log(res);
                that.setData({
                    items:res.data.data
                })
            },
            fail: function(res) {     //失败回调
                console.log('fail')
            },
        });
    },
    addMeterials(){
        dd.navigateTo({
            url:'../addMeterials/addMeterials'
        })
    }
})