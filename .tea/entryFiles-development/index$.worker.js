require('./config$');

function success() {
require('../..//app');
require('../../node_modules/mini-ddui/es/tabs/index');
require('../../node_modules/mini-ddui/es/tabs/tab-content/index');
require('../../node_modules/mini-ddui/es/card/index');
require('../../components/login/login');
require('../../pages/index/index');
require('../../pages/addproject/index');
require('../../pages/member/index');
require('../../pages/message/index');
require('../../pages/myself/index');
require('../../components/usecourse/usecourse');
require('../../components/suggest/suggest');
require('../../components/register/register');
require('../../components/setMeterials/setMeterials');
require('../../components/addMeterials/addMeterials');
}
self.bootstrapApp ? self.bootstrapApp({ success }) : success();
